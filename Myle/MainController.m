//
//  ViewController.m
//  Myle
//
//  Created by Mikalai on 2014-03-26.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import "MainController.h"


@interface MainController ()

- (void) loadLanguageButton;

@end


@implementation MainController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.navigationController setNavigationBarHidden:YES animated:NO];

    self.data = [[MSRecordsSource alloc] initWithCallback:^
                 {
                     [self.tableView reloadData];
                 }];
    [self.tableView setDataSource:self.data];
    [self.tableView setDelegate:self];
    [self.tableView reloadData];

    self.recorder = [MSRecorderService alloc];
    [self.recorder setErrorHandler:^(NSError *error)
                            {
                                NSLog(@"%@", error.localizedDescription);
                            }];
}


- (void)viewWillAppear:(BOOL)animated
{
    [self loadLanguageButton];
}


- (void) loadLanguageButton
{
    NSInteger lang = [[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    UIImage *buttonImage = [UIImage imageNamed: (lang == 1) ? @"En" : @"Ru"];
    [self.languageButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.recorder stop];

    self.alert = [[UIAlertView alloc] initWithTitle:nil
                                            message:@"Processing..."
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:nil];
    [self.alert show];
    
    NSInteger lang = [[NSUserDefaults standardUserDefaults] integerForKey:@"language"];

    MSNuanceService *nuance = [MSNuanceService alloc];
    [nuance recognize:self.recorder.file
             language: lang == 0 ? @"rus-RUS" : @"eng-USA"
               onText:^(NSString *text)
               {
                   NSLog(@"%@", text);

                   [self.data addRecord:text date:self.recorder.lastDate location:self.recorder.lastLocation language:lang];

                   [self.alert dismissWithClickedButtonIndex:-1 animated:NO];
                   MSAnalysisService *cloud = [[MSAnalysisService alloc] init];
                   [cloud newRecord:self.recorder.file
                           withText:text
                            onError:^(NSError *error)
                            {
                                NSLog(@"%@", error.localizedDescription);
                            }];
               }
              onError:^(NSError *error)
              {
                  [self.alert dismissWithClickedButtonIndex:-1 animated:NO];
                  NSLog(@"%@", error.localizedDescription);
              }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)languageButton:(id)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Choose recognition language"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Русский", @"English", nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0 || buttonIndex == 1)
    {
        [[NSUserDefaults standardUserDefaults] setInteger:buttonIndex forKey:@"language"];
        [self loadLanguageButton];
    }
}


- (IBAction)micButton:(id)sender {
    self.alert = [[UIAlertView alloc] initWithTitle:nil
                                            message:@"Speek now..."
                                           delegate:self
                                  cancelButtonTitle:@"Stop & Analyze"
                                  otherButtonTitles:nil];
    [self.alert show];

    [self.recorder record];
}

- (IBAction)wrenchButton:(id)sender
{
    SettingsController *lvc = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsController"];
    //[self presentViewController:lvc animated:YES completion:nil];
    [self.navigationController pushViewController:lvc animated:YES];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RecordDetailsController *c = [self.storyboard instantiateViewControllerWithIdentifier:@"RecordDetailsController"];
    c.record = [self.data getRecord:indexPath];
    c.data = self.data;
    [self.navigationController pushViewController:c animated:YES];
}


@end
