//
//  ViewController.h
//  Myle
//
//  Created by Mikalai on 2014-03-26.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MSRecorderService.h"
#import "MSAnalysisService.h"
#import "MSNuanceService.h"
#import "MSRecordsSource.h"
#import "SettingsController.h"
#import "RecordDetailsController.h"


@interface MainController : UIViewController <UITableViewDelegate, UIActionSheetDelegate>

@property MSRecordsSource *data;
@property MSRecorderService *recorder;
@property UIAlertView *alert;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *busyIndicator;
@property (weak, nonatomic) IBOutlet UIButton *languageButton;

- (IBAction)languageButton:(id)sender;
- (IBAction)micButton:(id)sender;
- (IBAction)wrenchButton:(id)sender;

@end
