//
//  AppDelegate.h
//  Myle
//
//  Created by Mikalai on 2014-03-26.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
