//
//  MSRecordsSource.m
//  Myle
//
//  Created by Mikalai on 2014-04-12.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import "MSRecordsSource.h"


@interface MSRecordsSource()

-(void) save;

@end



@implementation MSRecordsSource


- (id) initWithCallback:(SourceChangedCallback)changeCallback
{
    _changeCallback = changeCallback;
    
    // Point to Document directory
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    self.file = [documentsDirectory stringByAppendingPathComponent:@"/records.json"];

    NSMutableArray *array = [[NSMutableArray alloc] init];
    self.records = array;

    if([[NSFileManager defaultManager] fileExistsAtPath:self.file])
    {
        NSInputStream *stream = [NSInputStream inputStreamWithFileAtPath:self.file];
        [stream open];

        if (stream) {
            NSError *parseError = nil;
            NSArray *a = [NSJSONSerialization JSONObjectWithStream:stream options:NSJSONReadingAllowFragments error:&parseError];
            for (NSUInteger i = 0; i < a.count; ++i) {
                NSDictionary *r = [a objectAtIndex:i];
                Record *rec = [[Record alloc] init:[r objectForKey:@"text"]
                                            date:[r objectForKey:@"date"]
                                             lng:[r objectForKey:@"lng"]
                                             lat:[r objectForKey:@"lat"]
                                              lang:[r objectForKey:@"lang"]];
                rec.lastUpdatedDate = [r objectForKey:@"lastUpdatedDate"];
                [array addObject:rec];
            }
        } else {
            NSLog(@"Failed to open stream.");
        }
    }

    return self;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = (indexPath.row % 2) ? @"Cell" : @"AltCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        //cell.backgroundColor = (indexPath.row % 2) ? [UIColor colorWithRed:1 green:0 blue:0 alpha: 0] : [UIColor colorWithRed:0 green:1 blue:0 alpha: 0];
    }
    Record *record = [self.records objectAtIndex:indexPath.row];
	cell.detailTextLabel.text = record.text;
	return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.records.count;
}


- (void) addRecord:(NSString*)text date:(NSDate*)date location:(CLLocation*)location language:(NSInteger)language
{
    // add record to array
    NSMutableArray *array = (NSMutableArray*)self.records;
    [array insertObject:[[Record alloc] init:text
                                        date:[self getUTCFormateDate:date]
                                         lng:[NSNumber numberWithDouble: location.coordinate.longitude]
                                         lat:[NSNumber numberWithDouble: location.coordinate.latitude]
                                        lang:[NSNumber numberWithInteger:language]]
                atIndex:0];
    
    [self save];
    
     _changeCallback();
}


-(NSString *)getUTCFormateDate:(NSDate *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    return dateString;
}


- (Record*) getRecord:(NSIndexPath*)indexPath
{
    return [self.records objectAtIndex:indexPath.row];
}


- (void) removeRecord:(Record*)record
{
    NSMutableArray *array = (NSMutableArray*)self.records;
    [array removeObject:record];
 
    [self save];
    
    _changeCallback();
}

- (void) updateRecord:(Record*)record
{
    record.lastUpdatedDate = [self getUTCFormateDate:[NSDate date]];
    
    [self save];
    
    _changeCallback();
}

- (void) save
{
    // convert array of records to array of dictionaries to be serializable
    NSMutableArray *jsonArray = [[NSMutableArray alloc] init] ;
    for (NSUInteger i = 0; i < self.records.count; ++i)
    {
        Record *r = [self.records objectAtIndex:i];
        NSDictionary *d = [[NSDictionary alloc] initWithObjectsAndKeys:
                           r.text ?: [NSNull null], @"text",
                           r.date ?: [NSNull null], @"date",
                           r.lng ?: [NSNull null], @"lng",
                           r.lat ?: [NSNull null], @"lat",
                           r.lang ?: [NSNull null], @"lang",
                           r.lastUpdatedDate ?: [NSNull null], @"lastUpdatedDate", nil];
        [jsonArray addObject:d];
    }
    
    // save array to json file
    NSOutputStream *stream = [[NSOutputStream alloc] initToFileAtPath:self.file append:NO];
    [stream open];
    
    NSError *writeError = nil;
    NSInteger bytesWritten = [NSJSONSerialization writeJSONObject:jsonArray toStream:stream options:NSJSONWritingPrettyPrinted error:&writeError];
    
    if (bytesWritten <= 0) {
        NSLog(@"Error writing JSON Data");
    }
}

@end
