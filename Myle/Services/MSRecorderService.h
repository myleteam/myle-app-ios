//
//  MSRecorderService.h
//  Myle
//
//  Created by Mikalai on 2014-04-11.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>

@interface MSRecorderService : NSObject <CLLocationManagerDelegate>

@property CLLocation *lastLocation;
@property NSDate *lastDate;
@property CLLocationManager *locationManager;
@property NSString *file;
@property (strong, nonatomic) AVAudioRecorder *audioRecorder;

- (void) setErrorHandler:(void (^)(NSError*))onError;
- (BOOL) record;
- (void) stop;

@end
