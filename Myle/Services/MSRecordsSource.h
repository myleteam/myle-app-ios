//
//  MSRecordsSource.h
//  Myle
//
//  Created by Mikalai on 2014-04-12.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Record.h"

typedef void (^SourceChangedCallback)();

@interface MSRecordsSource : NSObject <UITableViewDataSource>
{
    SourceChangedCallback _changeCallback;
}
@property NSString *file;
@property NSArray *records;

- (id) initWithCallback:(void (^)())changeCallback;

- (void) addRecord:(NSString*)text date:(NSDate*)date location:(CLLocation*)location language:(NSInteger)language;
- (void) updateRecord:(Record*)record;
- (void) removeRecord:(Record*)record;

- (Record*) getRecord:(NSIndexPath*)indexPath;

@end
