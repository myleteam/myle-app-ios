//
//  MSNuanceService.m
//  Myle
//
//  Created by Mikalai on 2014-04-08.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import "MSNuanceService.h"

@implementation MSNuanceService

- (void)recognize:(NSString*)record
         language:(NSString*)lang
           onText:(void (^)(NSString*))onText
          onError:(void (^)(NSError*))onError
{
    NSURL *url = [NSURL URLWithString:@"https://dictation.nuancemobility.net/NMDPAsrCmdServlet/dictation?appId=NMDPTRIAL_mikalai_getmyle_com20140327213836&appKey=3f5b0f50b8a3c23fc2361c0ab1cf25302b0d611bb86f95100712e73019f068584c77a8041a6b43dca15fe7522e0c82591ae375d2530ae35a1cca8a36baf38e06&id=3628"];
    NSDictionary *headers = [[NSDictionary alloc] initWithObjectsAndKeys:
        @"audio/x-wav;codec=pcm;bit=16;rate=8000", @"content-type",
        @"text/plain", @"accept",
        lang, @"accept-language",
        @"Dictation", @"Accept-Topic",
        nil];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod: @"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBodyStream:[NSInputStream inputStreamWithFileAtPath:record]];

    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
    {
        if (error && onError)
        {
            onError(error);
        }
        else
        {
            NSHTTPURLResponse *r = (NSHTTPURLResponse*)response;
            NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            if (r.statusCode >= 200 && r.statusCode <= 300 && onText)
            {
                NSArray *lines = [result componentsSeparatedByString:@"\n"];
                if(lines.count > 0) {
                    onText(lines[0]);
                }
            }
            else if(onError)
            {
                // TODO: create NSError here
                onError(nil);
            }
        }
    }];
}

@end
