//
//  MSRecorderService.m
//  Myle
//
//  Created by Mikalai on 2014-04-11.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import "MSRecorderService.h"

@implementation MSRecorderService


- (void) setErrorHandler:(void (^)(NSError*))onError
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;

    // Point to Document directory
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    self.file = [documentsDirectory stringByAppendingPathComponent:@"/record.wav"];

    NSError *error = nil;

    AVAudioSession *s = [AVAudioSession sharedInstance];
    [s setCategory:AVAudioSessionCategoryRecord withOptions:AVAudioSessionCategoryOptionAllowBluetooth error:&error];
    [s setActive:YES error:&error];

    if(error) {
        onError(error);
    }

    // set up for bluetooth microphone input
   // UInt32 allowBluetoothInput = 1;
   // OSStatus stat = AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryEnableBluetoothInput, sizeof(allowBluetoothInput), &allowBluetoothInput);
    //NSLog(@"status = %x", stat);    // problem if this is not zero

    // check the audio route
  //  UInt32 size = sizeof(CFStringRef);
  //  CFStringRef route;
  //  OSStatus result = AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &size, &route);
    //NSLog(@"route = %@", route);
    // if bluetooth headset connected, should be "HeadsetBT"
    // if not connected, will be "ReceiverAndMicrophone"

    // define audio file url
    NSURL *audioFileURL = [[NSURL alloc] initFileURLWithPath:self.file];

    // define audio recorder settings
    NSDictionary *settings = [[NSDictionary alloc] initWithObjectsAndKeys:
       [NSNumber numberWithFloat: 8000], AVSampleRateKey,
       [NSNumber numberWithInt: kAudioFormatLinearPCM], AVFormatIDKey,
       [NSNumber numberWithInt:16], AVLinearPCMBitDepthKey,
       [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
       [NSNumber numberWithBool:NO], AVLinearPCMIsBigEndianKey,
       [NSNumber numberWithBool:NO], AVLinearPCMIsFloatKey,
       [NSNumber numberWithInt: AVAudioQualityLow], AVEncoderAudioQualityKey, nil];

    self.audioRecorder = [[AVAudioRecorder alloc]
        initWithURL:audioFileURL
        settings:settings
        error:&error];

    if(error) {
        onError(error);
    } else {
        [self.audioRecorder prepareToRecord];
    }
}


- (BOOL)record
{
    [self.locationManager startUpdatingLocation];
    return [self.audioRecorder record];
    return 1;
}


- (void)stop
{
    [self.audioRecorder stop];

    [self.locationManager stopUpdatingLocation];

    self.lastDate = [NSDate date];
}


//#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.lastLocation = newLocation;
}


@end
