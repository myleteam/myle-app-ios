//
//  MSAnalysisService.m
//  Myle
//
//  Created by Mikalai on 2014-04-08.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import "MSAnalysisService.h"

@implementation MSAnalysisService

- (void)newRecord:(NSString*)record withText:(NSString*)text onError:(void (^)(NSError*))onError
{/*
    NSURL *url = [NSURL URLWithString:@"http://myle.msilivonik.com/files/record"];
    NSMutableURLRequest *request = [[NSMutableURLRequest  alloc] initWithURL:url];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBodyStream:[NSInputStream inputStreamWithFileAtPath:record]];

    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
    {
        if (error != nil && onError != nil)
        {
            onError(error);
        }
    }];
  */
}

+ (void)export
{
    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *file = [documentsDirectory stringByAppendingPathComponent:@"records.json"];

    NSUUID *uid =[[UIDevice currentDevice] identifierForVendor];
    NSString *uidString = [uid UUIDString];

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://myle.msilivonik.com/files/json?id=%@", uidString]];
    NSMutableURLRequest *request = [[NSMutableURLRequest  alloc] initWithURL:url];
    [request setHTTPMethod: @"POST"];
    [request setHTTPBodyStream:[NSInputStream inputStreamWithFileAtPath:file]];

    [NSURLConnection sendAsynchronousRequest: request
                                       queue: [NSOperationQueue mainQueue]
                           completionHandler: ^(NSURLResponse *response, NSData *data, NSError *error)
    {
        if (!error)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                            message:@"Successfully exported!"
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                            message:@"Export failed!!!"
                                           delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  otherButtonTitles:nil];
            [alert show];
        }
    }];
}

@end
