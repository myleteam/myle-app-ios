//
//  MSNuanceService.h
//  Myle
//
//  Created by Mikalai on 2014-04-08.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MSNuanceService : NSObject

- (void)recognize:(NSString*)record
         language:(NSString*)lang
           onText:(void (^)(NSString*))onText
          onError:(void (^)(NSError*))onError;

@end
