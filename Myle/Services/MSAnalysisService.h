//
//  MSAnalysisService.h
//  Myle
//
//  Created by Mikalai on 2014-04-08.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MSAnalysisService : NSObject

- (void)newRecord:(NSString*)record withText:(NSString*)text onError:(void (^)(NSError*))onError;

+ (void)export;

@end
