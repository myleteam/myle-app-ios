//
//  Record.h
//  Myle
//
//  Created by Mikalai on 2014-04-07.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Record : NSObject

@property NSString *text;
@property NSString *date;
@property NSNumber *lng;
@property NSNumber *lat;
@property NSNumber *lang;
@property NSString *lastUpdatedDate;

- (id) init:(NSString*)text date:(NSString*)date lng:(NSNumber*)lng lat:(NSNumber*)lat lang:(NSNumber*)lang;

@end
