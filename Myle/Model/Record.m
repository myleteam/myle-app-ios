//
//  Record.m
//  Myle
//
//  Created by Mikalai on 2014-04-07.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import "Record.h"

@implementation Record

- (id) init:(NSString*)text date:(NSString*)date lng:(NSNumber*)lng lat:(NSNumber*)lat lang:(NSNumber*)lang
{
    self.text = text;
    self.date = date;
    self.lng = lng;
    self.lat = lat;
    self.lang = lang;

    return self;
}

@end
