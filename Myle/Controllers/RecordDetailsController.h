//
//  RecordDetailsController.h
//  Myle
//
//  Created by Mikalai on 2014-04-14.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Record.h"
#import "MSRecordsSource.h"
#import "MainController.h"

@interface RecordDetailsController : UIViewController<UIActionSheetDelegate, UITextViewDelegate>

@property MSRecordsSource *data;
@property Record *record;

- (IBAction)back:(id)sender;
- (IBAction)delete:(id)sender;
- (IBAction)edit:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *messageText;
@property (weak, nonatomic) IBOutlet UITextField *dateText;
@property (weak, nonatomic) IBOutlet UITextField *locationText;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
