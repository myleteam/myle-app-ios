//
//  SettingsController.h
//  Myle
//
//  Created by Mikalai on 2014-04-13.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSAnalysisService.h"

@interface SettingsController : UIViewController

- (IBAction)export:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *languageSegment;

- (IBAction)back:(id)sender;
@end
