//
//  SettingsController.m
//  Myle
//
//  Created by Mikalai on 2014-04-13.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import "SettingsController.h"

@interface SettingsController ()

@end

@implementation SettingsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];

    NSInteger lang = [[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    self.languageSegment.selectedSegmentIndex = lang;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)export:(id)sender {
    [MSAnalysisService export];
}


- (IBAction)back:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setInteger:self.languageSegment.selectedSegmentIndex forKey:@"language"];
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
