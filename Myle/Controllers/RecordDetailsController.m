//
//  RecordDetailsController.m
//  Myle
//
//  Created by Mikalai on 2014-04-14.
//  Copyright (c) 2014 Myle Electronics Corp. All rights reserved.
//

#import "RecordDetailsController.h"

@interface RecordDetailsController ()

- (void) registerForKeyboardNotifications;

@end

@implementation RecordDetailsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:self.record.date];

    timeZone = [NSTimeZone systemTimeZone];
    [dateFormatter setTimeZone:timeZone];

    self.messageText.text = self.record.text;
    self.dateText.text = [dateFormatter stringFromDate:date];//[NSString stringWithFormat:@"%@", date]; //self.record.date;
    self.locationText.text = [NSString stringWithFormat:@"%@, %@", self.record.lng, self.record.lat];
    [self.messageText setDelegate:self];
    
    [self registerForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}


// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)notification
{/*
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.messageText.frame.origin) ) {
        [self.scrollView scrollRectToVisible:self.messageText.frame animated:YES];
    }*/
    [UIView beginAnimations:nil context:nil];
    CGRect endRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect newRect = self.messageText.frame;
    //Down size your text view
    [self.scrollView setContentOffset:CGPointMake(0, newRect.origin.y) animated:NO];
    //newRect.size.height -= endRect.size.height;
    //ßself.messageText.frame = newRect;
    [UIView commitAnimations];
  }

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
  /*  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;*/
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound) {
        return YES;
    }
    
    [textView resignFirstResponder];
    [self.messageText setEditable:NO];
    [self.messageText setScrollEnabled:NO];
    self.scrollView.scrollEnabled = YES;
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    self.record.text = self.messageText.text;
    [self.data updateRecord:self.record];
    
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)delete:(id)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Confirm deletion"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:@"Delete"
                                              otherButtonTitles:nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [self.data removeRecord:self.record];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)edit:(id)sender
{
    [self.messageText setEditable:YES];
    [self.messageText setScrollEnabled:YES];
    [self.messageText becomeFirstResponder];
    
    self.scrollView.scrollEnabled = NO;
}
@end
